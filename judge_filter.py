#!/usr/bin/python3



#############################################
# Judge filter by catalog matching          #
# Made by R. Itoh (2019-03-07)              #
# Last update 2021-03-01                    #
#############################################

# ===============================================
# Imports  ======================================
# ===============================================

import sys,os,time,math,random,re
import urllib,getopt,string,optparse
import numpy
import astropy
import astropy.stats
import astropy.coordinates

# ===============================================
# Class UpperLimit  =============================
# ===============================================

class Judge_Filter():

    def __init__(self,fits):
        self.fits= fits
        pw_UCAC  = "/home/itoh/UCAC4"
        if os.path.exists(pw_UCAC)==False:
            print("No UCAC-4 catalog")
            return False
        sys.path.append(pw_UCAC)
        import phot_all
        self.ph = phot_all.Phot(fits)
        

    def sdss_r2R(self,r,i): #Lupton (2005)
        # vega 2 AB
        R = r - 0.2936*(r - i) - 0.1439 + 0.21
        return R
    
    def sdss_i2I(self,r,i): #Lupton (2005)
        I = r - 1.2444*(r - i) - 0.3820 + 0.45
        return I
    def sdss_g2B(self,g,r): #Lupton (2005)
        # Vega 2 AB
        B = g + 0.3130*(g - r) + 0.2271 - 0.09
        return B

    def sdss_g2V(self,g,r): #Lupton (2005)
        # Vega 2 AB
        V = g - 0.5784*(g - r) - 0.0038 + 0.02
        return V
    
    def make_comp(self,NS=1000,minmag=10.0,maxmag=16.0,Rad=0.3):
        self.ns   = {"B":7,"V":3,"g":10,"r":8,"i":9,
                     "J":4,"H":5,"Ks":6}
        import UCAC4_def
        CAT     = UCAC4_def.search_CAT_UCAC4(self.ph.RA,self.ph.Dec,float(Rad))
        self.P1 = []
        for n in range(len(CAT[0])):
            if float(minmag) > CAT[self.ns["V"]][n] or CAT[self.ns["V"]][n] > float(maxmag):continue
            g,r,i  = CAT[self.ns["g"]][n],CAT[self.ns["r"]][n],CAT[self.ns["i"]][n] ## AB magnitude
            R      = self.sdss_r2R(r,i)                                 ## AB mag
            I      = self.sdss_i2I(r,i)                                 ## AB mag
            B      = self.sdss_g2B(g,r)
            V      = self.sdss_g2V(g,r)
            #B      = CAT[self.ns["B"]][n]  -0.09 ## AB system
            #V      = CAT[self.ns["V"]][n]  +0.02 ## AB system
            J      = CAT[self.ns["J"]][n]  +0.91 ## AB system
            H      = CAT[self.ns["H"]][n]  +1.39 ## AB system
            Ks     = CAT[self.ns["Ks"]][n] +1.85 ## AB system 
            BV     = B-V
            ra     = float(CAT[1][n])
            dec    = float(CAT[2][n])
            self.P1.append({"g":g,"r":r,"i":i,
                            "B":B,"V":V,"R":R,"I":I,
                            "J":J,"H":H,"Ks":Ks,"B-V":BV,
                            "ra":ra,"dec":dec})


    def judge(self,cat):
        resL = self.ph.read_sextcat(cat)
        mB,mV,mR,mI = [],[],[],[]
        for res in resL:
            cra,cdec = res["ALPHA_J2000"],res["DELTA_J2000"]
            for p in self.P1:
                dis = self.ph.calc_cone_dis(cra,cdec,p["ra"],p["dec"])
                if 3600.0*dis < 1.5:
                    try:mB.append(res["MAG_AUTO"] - p["B"])
                    except:pass
                    try:mV.append(res["MAG_AUTO"] - p["V"])
                    except:pass
                    try:mR.append(res["MAG_AUTO"] - p["R"])
                    except:pass
                    try:mI.append(res["MAG_AUTO"] - p["I"])
                    except:pass
        band = ["B","V","R","I"]            
        sL = [astropy.stats.mad_std(mB),astropy.stats.mad_std(mV),astropy.stats.mad_std(mR),astropy.stats.mad_std(mI)]
        self.std_min  = min(sL)
        self.star_num = len(mR)
        return band[sL.index(min(sL))]
        



if __name__ == "__main__":
    fits = sys.argv[1]
    jd   = Judge_Filter(fits)
    jd.make_comp()
    jd.ph.mes_fwhm()
    cat = jd.ph.calc_zeromag()
    cat = jd.ph.phot_allstar()
    #cat  = "F1_RGB_R_phot.cat"
    band = jd.judge(cat)
    print(band)
