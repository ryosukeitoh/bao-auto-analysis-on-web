#!/usr/bin/python3

import os
import astropy
import astropy.io.fits
import sys,time,cgi,json,numpy,math
import cgi
import cgitb
import lacosmic
from subprocess import getoutput
#cgitb.enable()
## Original tools  ###############

pw_tool  = "/usr/lib/cgi-bin/calc_skylum"
pw_UCAC  = "/home/itoh/UCAC4"
sys.path.append(pw_UCAC)
import judge_filter
import phot_all


import traceback

class skylum():
    
    def __init__(self,fits):
        self.astrometry  = "nice -n +19 timeout 60 /usr/bin/solve-field %s -N %s -p -O "
        self.fits = fits
        self.jsonfile = fits.replace(".fits",".json")
        self.fwhm = False
        self.maxsize = 6400
        self.sigma   = 5.0
        self.v = ""
        
    def __def__(self):
        if self.status["finish"] != "true":
            self.status["finish"] = "true"
            self.status["success"] = "false"
            self.status["wcsfits"] = "false"
            self.status["regfile"] = "false"
            self.write_json()
            
    def load_json(self):
        k           = open(self.jsonfile,"r")
        self.status = json.loads(k.read())
        k.close()

    def write_json(self):
        w = open(self.jsonfile,"w")
        json.dump(self.status,w)
        w.close()
    
    def check_fits(self):
        if os.path.exists(self.jsonfile):
            self.jsonfileflag = True
        else:
            self.jsonfileflag = False
        try:
            fd = astropy.io.fits.open(self.fits)
            h  = fd[0].header
            mxy = fd[0].data.shape
            print(len(mxy))
            print(len(fd))
            #if len(fd) >= 2 or len(mxy)>=3:
            if len(mxy)>=3:
                print("Color/Multi-FITS")
                self.v = "Faild: Color or Multi-FITS"
                self.readfitsflag = False
            else:
                D     = fd[0].data
                mx,my = D.shape
                print(mx,my)
                if mx > self.maxsize or my > self.maxsize:
                    print("Too Large")
                    self.v = "Faild: FITS is too Large"
                    self.readfitsflag = False
                else:
                    self.readfitsflag = True
            fd.close()
            if "CTYPE1" in h.keys():
                self.wcsheadflag = True
            else:
                self.wcsheadflag = False
        except:
            traceback.print_exc()
            self.v = "Can't read FITS"
            self.readfitsflag = False
            self.wcsheadflag  = False
            

    def remove_cosmic(self):
        nfits = self.fits.replace(".fits","_lc.fits")
        if os.path.exists(nfits):
            self.fits = nfits
            return nfits
        try:
            fd   = astropy.io.fits.open(self.fits)
            hD   = fd[0].data
            D    = hD.astype(numpy.float)
            H    = fd[0].header
            nD   = lacosmic.lacosmic(D,2.0,3.0,3.0,
                                      readnoise=10.0,
                                      effective_gain=1.0)[0]
            hdu     = astropy.io.fits.PrimaryHDU(nD,H)
            hdulist = astropy.io.fits.HDUList([hdu])
            hdulist.writeto(nfits,output_verify='ignore')
            fd.close()
            if os.path.exists(nfits):
                self.fits = nfits
                return True
            else:
                return False
        except:
            traceback.print_exc()
            return False
        
    def wcs_astrometry(self):
        wcsfits = self.fits.replace(".fits","_wcs.fits")
        if os.path.exists(wcsfits):return wcsfits
        if self.status["ra"] != "false" and self.status["dec"] != "false":
            self.astrometry += " --ra %s --dec %s --radius 1.0 "%(self.status["ra"],self.status["dec"])
        if self.status["pixelscale"] != "false":
            cS      = float(self.status["pixelscale"])
            upS,lwS = 1.2*cS,0.8*cS
            self.astrometry += "-L %s -H %s -u arcsecperpix "%(lwS,upS)
        try:
            l = getoutput(self.astrometry%(self.fits,wcsfits))
        except:
            traceback.print_exc()
            return False
        if os.path.exists(wcsfits):return wcsfits
        else:return False

    def set_wcs(self,wcsfits):
        self.wcsfits = wcsfits
        self.jd      = judge_filter.Judge_Filter(wcsfits)
        self.ph      = phot_all.Phot(wcsfits)
        
    def judge_band(self):
        try:
            self.jd.make_comp()
            self.ph.mes_fwhm()
            self.fwhm = self.ph.fwhm
            self.ph.calc_zeromag()
            cat  = self.ph.phot_allstar()
            self.eband = self.jd.judge(cat)
        except:
            traceback.print_exc()
            return False
        return True
    
    def set_header(self,keyword,value):
        fd = astropy.io.fits.open(self.wcsfits)
        fd[0].header[keyword] = value
        fd.writeto(self.wcsfits,overwrite=True)
        fd.close()
                                
    def calc_zeromag(self):
        try:
            self.ph = phot_all.Phot(self.wcsfits)
            if self.fwhm == False:
                self.ph.mes_fwhm()
                self.fwhm = self.ph.fwhm
            self.ph.fwhm      = self.fwhm
            self.ph.calc_zeromag()
        except:
            traceback.print_exc()
            return False
        return True
        
    def all_phot(self):
        cat = self.ph.phot_allstar()
        self.ph.sigma = self.sigma
        try:
            self.ph.upperlimit(cat)
        except:
            self.ph.uplim = -999
        if self.ph.uplim != -999:
            self.ph.th_mag = 25.0
        else:
            self.ph.th_mag = self.ph.uplim
            
        self.ph.make_region(cat)
        self.ph.make_region_short(cat,30)
        
    def calc_skymag(self):
        fd  = astropy.io.fits.open(self.wcsfits)
        D   = fd[0].data
        br  = numpy.median(D)
        c1  = fd[0].header["CD1_1"]
        c2  = fd[0].header["CD1_2"]
        self.ra       = fd[0].header["CRVAL1"]
        self.dec      = fd[0].header["CRVAL2"]
        self.pixscale = math.sqrt(c1**2+c2**2)*3600.0 # arcsec/pix
        self.skycnt   = br/self.pixscale**2
        self.skymag   = -2.5*math.log(self.skycnt,10)+self.zmag
            

        
        
    def run(self):
        print("check fits")
        ## Check FITS file and JSON file
        self.check_fits()
        if self.jsonfileflag==False:
            self.status = {"r":"Error","v":"No status file"}
            self.write_json()
            return False
        self.load_json()
        if self.status["finish"] == "true":return False
        if self.readfitsflag:
            self.status["check"] = "Success"
            self.write_json()
        else:
            self.status["check"] = "Failed:"+self.v
            self.write_json()
            return False
        ## Lacosmic
        print("LACOSMIC")
        self.status["lacos"] = "Analyzing..."
        self.write_json()
        rc = self.remove_cosmic()
        if rc:
            self.status["lacos"] = "Success"
            self.write_json()   
        else:
            self.status["lacos"] = "Failed"
            self.write_json()
            return False
        print("WCS")
        ## Astrometry
        if self.wcsheadflag:
            self.set_wcs(self.fits)
            self.status["wcs"] = "Skipped"
            self.write_json()
            wcs = self.fits
        else:
            self.status["wcs"] = "Analyzing..."
            self.write_json()
            wcs = self.wcs_astrometry()
        if wcs == False:
            self.status["wcs"] = "Failed"
            self.write_json()
            return False
        else:
            if self.wcsheadflag==False:
                self.status["wcs"] = "Success"
                self.write_json()
        self.set_wcs(wcs)
        # Filter estimation
        print("FILTER")
        self.status["filter_estimation"] = "Analyzing..."
        self.write_json()        
        if self.status["filter"]!="unknown" and self.status["filter"]!="false":
            self.eband = self.status["filter"]
            self.status["filter_estimation"] = "Skipped:"+self.status["filter"]
            self.status["res_filter"] = self.eband
            self.write_json()
            self.set_header("FIL-PHOT",self.eband)
        else:
            jb = self.judge_band()
            if jb:
                self.set_header("FIL-PHOT",self.eband)
                self.status["filter_estimation"] = "Success:"+self.eband
                self.status["res_filter"] = self.eband
                self.write_json()
            else:
                self.status["filter_estimation"] = "Failed"
                self.write_json()
                return False
        print("calc zeromag")
        # calc zeromag
        self.status["zeromag"] = "Analyzing..."
        self.status["uplim"] = "Analyzing..."
        self.write_json()
        
        cz = self.calc_zeromag()
        if cz:
            self.zmag = self.ph.zmag
            self.all_phot()
            self.set_header("BAO-ZMG",self.zmag)
            self.status["res_zmag"] = self.zmag
            self.status["res_uplim"] = self.ph.uplim
            self.status["zeromag"] = "Success:%1.2f"%self.zmag
            self.write_json()
        else:
            self.status["zeromag"] = "Failed"
            self.write_json()
            return False

        # skymag
        self.status["skymag"] = "Analyzing..."
        self.write_json()
        try:
            self.calc_skymag()
            self.status["res_skymag"] = self.skymag
            self.status["res_skycnt"] = self.skycnt
            self.status["res_fwhm"]   = self.fwhm*self.pixscale
            self.status["res_pixelscale"] = self.pixscale
            self.status["res_ra"]     = self.ra
            self.status["res_dec"]    = self.dec            
            self.status["skymag"]     = "Success:%1.2f"%(self.skymag)
            self.write_json()
        except:
            self.status["skymag"] = "Failed"
            self.write_json()
            return False
        return True
                
if __name__ == "__main__":

    fits = sys.argv[1]

    sl   = skylum(fits)
    print("start")
    st = sl.run()
    if sl.status["finish"] == "false":
        sl.status["finish"] = "true"
        if st:
            sl.status["success"] = "true"
            sl.status["wcsfits"] = sl.wcsfits
            sl.status["regfile"] = sl.wcsfits.replace(".fits","_phot.reg")
            sl.status["sregfile"] = sl.wcsfits.replace(".fits","_phot_short.reg")
        else:
            sl.status["success"] = "false"
            sl.status["wcsfits"] = "false"
            sl.status["regfile"] = "false"
        sl.write_json()
    
