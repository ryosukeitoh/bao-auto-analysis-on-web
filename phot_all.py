#!/usr/bin/python3



#############################################
# Aperture photometry w/ sextractor         #
# Made by R. Itoh (2019-02-19)              #
# Last update 2019-02-19                    #
#############################################

# ===============================================
# Imports  ======================================
# ===============================================

import sys,os,time,math,random,re
import urllib,getopt,string,optparse
import numpy
import astropy
import astropy.stats
import astropy.coordinates
import photutils

from subprocess import getoutput

# ===============================================
# Class UpperLimit  =============================
# ===============================================

class Phot():

    def __init__(self,fits):

        if ".fits" not in fits:
            print(fits,"is not FITS file (use .fits extension)")
        self.fits      = fits
        self.sextract  = "/usr/bin/sextractor"
        idir           = "/set_your_working_dir/"
        self.sexconv   = idir + "default.conv"  # parameter file 
        self.sexconf   = idir + "default.sex"   # parameter file 
        self.sexparam  = idir + "default.param" # parameter file 
        self.sextmp    = "test.cat"             # default name of outputfile made by sextractor
        self.Apature   = 5.0
        self.sky_width = 3.0
        self.sigma     = 5.0
        fd     = astropy.io.fits.open(self.fits)
        hd     = fd[0].header
        self.data = fd[0].data
        xmax,ymax= fd[0].data.shape
        cx,cy  = xmax/2.0,ymax/2.0
        twcs   = astropy.wcs.WCS(self.fits)
        cpos   = twcs.all_pix2world([cx],[cy],1)
        self.RA,self.Dec = cpos[0][0],cpos[1][0]
        self.num_aper = 1000.0
        self.minX,self.maxX = int(0.1*xmax),int(0.9*xmax)
        self.minY,self.maxY = int(0.1*ymax),int(0.9*ymax)
        self.th_mag = 50.0
        if   "FIL-PHOT" in hd:self.fil = fd[0].header["FIL-PHOT"]
        elif "FIL-NAME" in hd:self.fil = fd[0].header["FIL-NAME"]
        else:self.fil = "r"
        
        fd.close()
        
        pw_UCAC  = "/set_UCAC4_catalog_dir"
        if os.path.exists(pw_UCAC)==False:
            print("No UCAC-4 catalog")
            return False
        sys.path.append(pw_UCAC)
        self.zmag = 0.0 

    def sextract_param(self):
        k = open(self.sexparam,"w")
        k.write("ALPHA_J2000\n")
        k.write("DELTA_J2000\n")
        k.write("X_IMAGE\n")
        k.write("Y_IMAGE\n")
        k.write("FWHM_IMAGE\n")
        k.write("MAG_AUTO\n")
        k.write("MAGERR_AUTO\n")
        k.write("MAG_APER\n")
        k.write("MAGERR_APER\n")
        k.close()
            
    def sextract_conv(self):
        k = open(self.sexconv,"w")
        k.write("CONV NORM\n")
        k.write("1 2 1\n")
        k.write("2 4 2\n")
        k.write("1 2 1\n")
        k.close()

    def sextract_conf(self):
        os.system(self.sextract+" -d > %s"%self.sexconf)
        k = open(self.sexconf,"r")
        r = k.read()
        k.close()
        k = open(self.sexconf,"w")
        repw1 = "MAG_ZEROPOINT  25.0"
        try:
            if 1.0 < self.zmag and self.zmag < 50.0:
                repw1 = "MAG_ZEROPOINT  %s"%self.zmag
            else:
                repw1 = "MAG_ZEROPOINT  0.0"
        except:
            repw1 = "MAG_ZEROPOINT  25.0"
        repw2 = "PHOT_APERTURES %1.2f"%(5.0*self.fwhm)
        for l in r.split("\n"):
            if "PHOT_APERTURES" in l:
                l = repw2
            if "MAG_ZEROPOINT" in l:
                l = repw1
                print(l)
            k.write("%s\n"%l)               
        k.close()

    def sdss_r2R(self,r,i): #Lupton (2005)
        # vega 2 AB
        R = r - 0.2936*(r - i) - 0.1439 + 0.21
        return R
    
    def sdss_i2I(self,r,i): #Lupton (2005)
        I = r - 1.2444*(r - i) - 0.3820 + 0.45
        return I

        
    def make_compf(self,NS=1000,minmag=10.0,maxmag=18.0,Rad=0.3):
        self.compf = self.fits.replace(".fits","_compf.dat")
        self.ns   = {"B":7,"V":3,"g":10,"r":8,"i":9,
                     "J":4,"H":5,"Ks":6}
        import UCAC4_def
        CAT    = UCAC4_def.search_CAT_UCAC4(self.RA,self.Dec,float(Rad))
        NL1    = [[],[]]
        pNL1   = []
        for i in range(len(CAT[0])):
            if float(minmag) < CAT[self.ns["V"]][i] and CAT[self.ns["V"]][i] < float(maxmag):
                ra   = float(CAT[1][i])
                dec  = float(CAT[2][i])
                pNL1.append((CAT[self.ns["V"]][i],CAT[0][i]))
                
        sortNL1 = sorted(pNL1)
        C1      = open(self.compf,"w")
        C1.write("# From UCAC-4 Catalog\n")
        C1.write("# comp B V g r i R I J H Ks B-V\n")
        sN1     = len(pNL1)
        if sN1 >= float(NS):
            sN1  = int(NS)
        for di1 in range(sN1):
            N1     = CAT[0].index(sortNL1[di1][1])
            g,r,i  = CAT[self.ns["g"]][N1],CAT[self.ns["r"]][N1],CAT[self.ns["i"]][N1] ## AB magnitude
            R      = self.sdss_r2R(r,i)                                 ## AB mag
            I      = self.sdss_i2I(r,i)                                 ## AB mag
            B      = CAT[self.ns["B"]][N1]  -0.09 ## AB system
            V      = CAT[self.ns["V"]][N1]  +0.02 ## AB system
            J      = CAT[self.ns["J"]][N1]  +0.91 ## AB system
            H      = CAT[self.ns["H"]][N1]  +1.39 ## AB system
            Ks     = CAT[self.ns["Ks"]][N1] +1.85 ## AB system 
            BV     = B-V
            ra     = float(CAT[1][N1])
            dec    = float(CAT[2][N1])
            cor    = astropy.coordinates.SkyCoord(ra,dec,unit='deg',
                                                  frame=astropy.coordinates.FK5)
            radec  = cor.to_string('hmsdms')
            C1.write("#comp%s: UCAC-4 %s %s\n"%(di1+1,CAT[0][N1],radec))
            C1.write("comp%s %s %s %s %s %s %s %s %s %s %s %s\n"%(di1+1,
                                                                  B,V,g,r,i,R,I,
                                                                  J,H,Ks,BV))
        C1.close()
        return self.compf

    def read_compf(self,F1):
        #P1,ra,dec = [],[],[]
        P1        = []
        k1        = open(F1,"r")
        r1        = k1.read().split("\n")
        k1.close()
        wcs       = astropy.wcs.WCS(self.fits)
        for i in range(len(r1)):
            d1 = r1[i].split()
            if len(d1) <= 0:continue
            if "#comp" in d1[0]:
                ms     = r1[i+1].split()
                cor    = astropy.coordinates.SkyCoord(d1[3],d1[4],
                                                      frame=astropy.coordinates.FK5)
                #ra.append(cor.ra.deg)
                #dec.append(cor.dec.deg)
                ms.insert(0,cor.dec.deg)
                ms.insert(0,cor.ra.deg)
                P1.append(ms)
        #xy = wcs.all_world2pix(ra,dec,1)
        #k  = open(self.posf,"w")
        #for i in range(len(xy[0])):
        #    k.write("%s %s\n"%(xy[0][i],xy[1][i]))        
        #k.close()
        return P1
        
    def read_sextcat(self,catf):
        k  = open(catf,"r")
        r  = k.read().split("\n")
        k.close()
        k2 = open(self.sexparam,"r")
        r2 = k2.read().split("\n")
        k2.close()
        params,allres = [],[]
        for x in r2:
            d = x.split()
            if len(d) != 0:
                params.append(d[0])
        for x in r:
            d = x.split()
            if "#" in d or len(d) == 0:continue
            nd = {}
            for i in range(len(params)):
                try:
                    nd[params[i]] = float(d[i])
                except:
                    nd[params[i]] = d[i]
            allres.append(nd)
        return allres
    
    def calc_cone_dis(self,RA1_1,DEC1_1,RA1_2,DEC1_2):
        x1_1 = math.cos(math.radians(DEC1_1))*math.cos(math.radians(RA1_1))
        y1_1 = math.cos(math.radians(DEC1_1))*math.sin(math.radians(RA1_1))
        z1_1 = math.sin(math.radians(DEC1_1))
        x1_2 = math.cos(math.radians(DEC1_2))*math.cos(math.radians(RA1_2))
        y1_2 = math.cos(math.radians(DEC1_2))*math.sin(math.radians(RA1_2))
        z1_2 = math.sin(math.radians(DEC1_2))
        d1   = math.sqrt((x1_1-x1_2)**2 + (y1_1-y1_2)**2 + (z1_1-z1_2)**2 )
        the1 = math.degrees(2*math.asin(d1/2.0))
        return the1

    def mes_fwhm(self):
        self.sextract_param()
        self.sextract_conv()
        l = getoutput(self.sextract+" "+self.fits+" -c "+self.sexconf+" > /dev/null")
        print(l)
        if os.path.exists(self.sextmp)==False:self.fwhm = 5.0
        resL     = self.read_sextcat(self.sextmp)
        allFWHM  = []
        for res in resL:
            allFWHM.append(res["FWHM_IMAGE"])
        #self.fwhm = astropy.stats.mad_std(allFWHM)
        self.fwhm = numpy.median(allFWHM)
        #print("\nFWHM = %1.2f pixel\n"%self.fwhm)
        
    def calc_zeromag(self,hname="BAO-ZMG",fname="FIL-ZMG"):
        rs     = {"B":3,"V":4,"G":5,"g":5,"r":6,
                  "i":7,"R":8,"I":9,
                  "J":10,"H":11,"Ks":12,
                  "Red":8,"Blue":3,"Green":4,"unknown":4,"Lunar":4,"L":4}
        self.sextract_param()
        self.sextract_conv()
        print("zeromag calc")
        self.sextract_conf()
        l = getoutput(self.sextract+" "+self.fits+" -c "+self.sexconf+" >/dev/null")
        if os.path.exists(self.sextmp)==False:return [[],[]]
        resL = self.read_sextcat(self.sextmp)
        self.compf = self.make_compf()
        comp       = self.read_compf(self.compf)
        zeromag    = [] 
        for res in resL:
            cra,cdec = res["ALPHA_J2000"],res["DELTA_J2000"]
            for p in comp:
                dis = self.calc_cone_dis(cra,cdec,p[0],p[1])
                if 3600.0*dis < 2.5:
                    #try:
                    zm =  float(p[rs[self.fil]]) - res["MAG_AUTO"]
                    #print(zm)
                    zeromag.append(zm)
                    #except:
                    #    pass
        try:
            self.zmag = numpy.median(zeromag)
        except:
            self.zmag = False
        
    def phot_allstar(self):
        cat = self.fits.replace(".fits","_phot.cat")
        l = getoutput("pwd")
        #self.mes_fwhm()
        self.sextract_param()
        self.sextract_conv()
        self.sextract_conf()
        #os.system(self.sextract+" "+self.fits+" > /dev/null")
        l = getoutput(self.sextract+" "+self.fits+" -c "+self.sexconf+" > /dev/null")
        if os.path.exists(self.sextmp)==False:return [[],[]]
        l = getoutput("cp %s %s"%(self.sextmp,cat))
        return cat
    
    def make_region(self,cat):
        reg = cat.replace(".cat",".reg") 
        k   = open(cat,"r")
        w   = open(reg,"w")
        r   = k.read().split("\n")
        w.write('global color=green font="helvetica 10 normal" ')
        w.write('select=1 highlite=1 edit=1 move=1 delete=1 ')
        w.write('include=1 fixed=0 source\nFK5\n')
        for l in r:
            d = l.split()
            if len(d) == 0:continue
            if "#" in l:continue
            try:
                p = list(map(float,d))
            except:
                continue
            w.write("point(%s,%s) # point=circle text={%1.2f +/- %1.2f}\n" %(p[0],
                                                                             p[1],
                                                                             p[5],
                                                                             p[6]))
        w.close()
        return reg

    def make_region_short(self,cat,num):
        reg = cat.replace(".cat","_short.reg") 
        k   = open(cat,"r")
        w   = open(reg,"w")
        r   = k.read().split("\n")
        w.write('global color=green font="helvetica 10 normal" ')
        w.write('select=1 highlite=1 edit=1 move=1 delete=1 ')
        w.write('include=1 fixed=0 source\nFK5\n')
        brL  =[]
        allL = []
        for l in r:
            d = l.split()
            if len(d) == 0:continue
            if "#" in l:continue
            try:
                p = list(map(float,d))
            except:
                continue
            brL.append(p[5])
            allL.append(p)
        sbrL = sorted(brL)
        cnt  = 0
        for s in sbrL:
            if cnt > num:break
            ids = brL.index(s)
            p   = allL[ids]
            if p[5] < self.th_mag:
                w.write("point(%s,%s) # point=circle text={%1.2f +/- %1.2f}\n" %(p[0],
                                                                                 p[1],
                                                                                 p[5],
                                                                                 p[6]))
            cnt += 1
        w.close()
        return reg


    def find_allstar(self,cat):
        resL = self.read_sextcat(cat)
        cl_wcs,cl_ps = [],[]
        allFWHM= []
        wcs = astropy.wcs.WCS(self.fits)
        for res in resL:
            ra,dec = wcs.all_pix2world(res["X_IMAGE"],res["Y_IMAGE"],0)
            cl_wcs.append([ra,dec])
        cl_ps.append([res["X_IMAGE"],res["Y_IMAGE"]])
        return [cl_wcs,cl_ps]
    
    def make_random_pos(self,cat):
        ps   = self.find_allstar(cat)
        self.aperture  = 1.25*self.fwhm
        cnt  = 0
        apos = []
        for tr in range(int(10*self.num_aper)):
            x    = random.uniform(self.minX,self.maxX)
            y    = random.uniform(self.minY,self.maxY)
            flag = True
            for xy in ps[1]:
                if math.sqrt((xy[0]-x)**2+(xy[1]-y)**2) < 10:
                    flag = False
                    break
            for p in apos:
                if  math.sqrt((p[0]-x)**2+(p[1]-y)**2) < 2*self.aperture:
                    flag = False
                    break
            if flag:
                apos.append([x,y])
                cnt += 1
            if cnt > float(self.num_aper):break
        return apos
                
                
    def upperlimit(self,cat):
        ps    = self.make_random_pos(cat)
        ap    = photutils.CircularAperture(ps, r=self.aperture)
        in_d  = 3*self.fwhm
        out_d = 3*self.fwhm + self.sky_width
        bg    = photutils.CircularAnnulus(ps, r_in=in_d, r_out=out_d)
        apf   = photutils.aperture_photometry(self.data, ap)
        bgf   = photutils.aperture_photometry(self.data, bg)
        #pt   = numpy.hstack([apf, bgf], table_names=['ap', 'bk'])
        apt   = numpy.hstack(apf['aperture_sum'])
        bgt   = numpy.hstack(bgf['aperture_sum'])
        bg_sum = ap.area()*bgt / bg.area()
        ap_sum = apt - bg_sum
        numpy.seterr(invalid='ignore')
        ap_sum2     = ap_sum[~numpy.isnan(ap_sum)]
        std         = astropy.stats.mad_std(ap_sum2)
        self.uplim  = -2.5*math.log((self.sigma*std),10)+self.zmag        
        
        
if __name__ == "__main__":
    fits = sys.argv[1]
    ph   = Phot(fits)
    ph.mes_fwhm()
    cat = ph.calc_zeromag()
    cat = ph.phot_allstar()
    ph.make_region(cat)
    ph.upperlimit(cat)
    print(ph.uplim)
