#!/usr/bin/python 

import sys,struct,math

tooldir = "./"
bipwd   = tooldir + "binary/"
indexf  = tooldir + "index_UCAC4_itoh.dat"
cm0     = [4,4,2,2,1,1,1] # ra,dec,magm,maga,simag,objt,cdf :15byte
cm1     = [1,1,1,1,1]     # sigra,sigdc,na1,nu1,cu1 :5byte
cm2     = [2,2,2,2,1,1]   # cepa,cepdc,pmrac,pmdc,sigpmr,sigpmd :10byte
cm3     = [4,2,2,2,1,1,1,1,1,1]   # pts_key,j_m,h_m,k_m,icqflg,2,3,e2mpho,2,3
cm4     = [2,2,2,2,2,1,1,1,1,1,1] # B,V,g,r,i,g,i,Be,Ve,ge,re,ie,gcflg
cm5     = [4]             # icf1-9
cm6     = [1,1,4,2,4]     #leda,x2m,rnm,zn2,rn2  
cm      = [cm0,cm1,cm2,cm3,cm4,cm5,cm6]

def search_CAT_individual(Bf,starti,endi,cRA,cDec,Rad):
    B       = open(Bf,"rb")
    d       = B.read(starti*78)
    P       = []
    n       = starti*78
    while n < endi*78:
        tP = []
        for j in range(len(cm)):    
            for k in range(len(cm[j])):
                if cm[j][k] == 4:
                    code = 'i'
                elif cm[j][k] == 2:
                    code = 'h'
                else:
                    code = 'b'
                n  = n + cm[j][k]
                tP.append(struct.unpack(code,B.read(int(cm[j][k])))[0])
        if calc_cone_dis(cRA,tP[0]/3600.0/1000.0,cDec,tP[1]/3600.0/1000.0-90.0) < Rad:
            P.append(tP)
    B.close()
    return P
def calc_cone_dis(RA1_1,RA1_2,DEC1_1,DEC1_2):
    x1_1 = (math.cos(((math.pi)*DEC1_1/180.0)))*(math.cos(((math.pi)*RA1_1/180.0)))
    y1_1 = (math.cos(((math.pi)*DEC1_1/180.0)))*(math.sin(((math.pi)*RA1_1/180.0)))
    z1_1 = (math.sin(((math.pi)*DEC1_1/180.0)))
    x1_2 = (math.cos(((math.pi)*DEC1_2/180.0)))*(math.cos(((math.pi)*RA1_2/180.0)))
    y1_2 = (math.cos(((math.pi)*DEC1_2/180.0)))*(math.sin(((math.pi)*RA1_2/180.0)))
    z1_2 = (math.sin(((math.pi)*DEC1_2/180.0)))
    d1   = math.sqrt( (x1_1-x1_2)**2 + (y1_1-y1_2)**2 + (z1_1-z1_2)**2 )
    the1 = 2*math.asin(d1/2.0)*180.0/math.pi
    return the1
def deg2ra(deg1):
    hour1 = math.floor(deg1/15.0)
    min1  = math.floor((deg1 - hour1*15.0)/15.0*60)
    sec1  = (deg1 - hour1*15.0 - min1*15.0/60.0)/15.0*3600.0
    timedis1 = "%02d:%02d:%02d.0"%(hour1,min1,sec1) 
    return timedis1
def deg2dec(deg1):
    if deg1 > 0:
        dec1  = math.floor(deg1)
        min1  = math.floor((deg1 - dec1)*60)
        sec1  = (deg1 - dec1 - min1/60.0)*3600.0
        timedis1 = "+%02d:%02d:%02d.0"%(dec1,min1,sec1)
    else:
        deg1  = -1*deg1
        dec1  = math.floor(deg1)
        min1  = math.floor((deg1 - dec1)*60)
        sec1  = (deg1 - dec1 - min1/60.0)*3600.0
        timedis1 = "-%02d:%02d:%02d.0"%(dec1,min1,sec1)
    return timedis1
def ra2deg(RA1):
    SS1    = RA1.split(":")
    return float(SS1[0])*15 + float(SS1[1])*15/60.0 + float(SS1[2])*15/3600.0
def dec2deg(DEC1):
    SS1 = DEC1.split(":")
    if float(SS1[0]) >= 0:
        DECDEG1 = float(SS1[0]) + float(SS1[1])/60.0 + float(SS1[2])/3600.0
    else:
        DECDEG1 = float(SS1[0]) - float(SS1[1])/60.0 - float(SS1[2])/3600.0
    return DECDEG1
def search_index_old(RA1,Dec1,R1):
    k1     = open(indexf,"r")
    D1     = k1.read().split("\n")
    k1.close()
    NL1    = []
    dnstep = 0.1 # deg
    for di in range(len(D1)-1):
        if "#" not in D1[di]:
            d1     = D1[di].split()
            ora    = -2*R1
            odec   = -2*R1
            while ora < 2*R1:
                while odec < 2*R1:
                    if RA1 + ora < 0:
                        hR = RA1 + ora + 360.0
                    elif RA1 + ora > 360.0:
                        hR = RA1 + ora - 360.0
                    else:
                        hR = RA1 + ora
                    if Dec1 + odec < -90.0 or Dec1 + odec > 90.0:
                        if hR + 180.0 > 360.0:
                            hR  = hR - 180.0
                        else:
                            hR  = hR + 180.0
                        if Dec1 + odec < -90.0:
                            hD = -90 + (abs(odec) - abs(90.0 - Dec1)) 
                        else:
                            hD =  90 - (abs(odec) - abs(90.0 - Dec1)) 
                    else:
                        hD  = Dec1 + odec
                    if float(d1[0]) < hR and hR <  float(d1[1]) and float(d1[2]) - 90.0 < hD and hD < float(d1[3]) - 90.0\
                            and [float(d1[0]),float(d1[1]),float(d1[2]),float(d1[3]),d1[4],int(d1[5]),int(d1[6])] not in NL1:
                        NL1.append([float(d1[0]),float(d1[1]),float(d1[2]),float(d1[3]),d1[4],int(d1[5]),int(d1[6])])
                    odec = odec + dnstep
                ora = ora + dnstep
    return NL1

def search_index(RA1,Dec1,R1):
    k1     = open(indexf,"r")
    D1     = k1.read().split("\n")
    k1.close()
    NL1    = []
    dnstep = 0.1 # deg
    for di in range(len(D1)-1):
        if "#" not in D1[di]:
            d1       = D1[di].split()
            midRA    = (float(d1[0]) + float(d1[1]) )/2.0
            midDec   = (float(d1[2]) + float(d1[3]) )/2.0 - 90.0
            if calc_cone_dis(RA1,midRA,Dec1,midDec) < R1 + 5.0:
                sRA      = float(d1[0])
                stopRA   = float(d1[1])
                c1       = 0
                while sRA <= stopRA:
                    sDec     = float(d1[2])-90.0
                    stopDec  = float(d1[3])-90.0
                    while sDec <= stopDec:
                        if calc_cone_dis(RA1,sRA,Dec1,sDec) < R1:
                            c1 = 1 
                        sDec = sDec + dnstep
                    sRA = sRA + dnstep
                if c1 == 1:
                    NL1.append([float(d1[0]),float(d1[1]),float(d1[2]),float(d1[3]),d1[4],int(d1[5]),int(d1[6])])
    return NL1
  
#################################################################
#################################################################
#################################################################
def search_CAT_UCAC4(cRA2,cDec2,Rad2):
    infs     = search_index(cRA2,cDec2,Rad2)
    p        = []
    for di2 in range(len(infs)):
        #print infs[di2][4]
        p.append(search_CAT_individual(bipwd+infs[di2][4],infs[di2][5],infs[di2][6],cRA2,cDec2,Rad2))
    ras      = []
    decs     = []
    Vmag     = []
    Jmag     = []
    Hmag     = []
    Kmag     = []
    Bmag     = []
    Rmag     = []
    Imag     = []
    gmag     = []
    ids      = []
    for i2 in range(len(p)):
        for j2 in range(len(p[i2])):
            ras.append(p[i2][j2][0]/3600.0/1000.0)
            decs.append(p[i2][j2][1]/3600.0/1000.0-90.0)
            Vmag.append(p[i2][j2][29]/1000.0)
            Jmag.append(p[i2][j2][19]/1000.0)
            Hmag.append(p[i2][j2][20]/1000.0)
            Kmag.append(p[i2][j2][21]/1000.0)
            Bmag.append(p[i2][j2][28]/1000.0)
            Rmag.append(p[i2][j2][31]/1000.0)
            Imag.append(p[i2][j2][32]/1000.0)
            gmag.append(p[i2][j2][30]/1000.0)
            ids.append(p[i2][j2][-3])
    return [ids,ras,decs,Vmag,Jmag,Hmag,Kmag,Bmag,Rmag,Imag,gmag]


#################################################################
def search_CAT_UCAC4_WCS(cRA3,cDec3,Rad3):
    CA3  = search_CAT_UCAC4(cRA3,cDec3,Rad3)
    nl3  = [[],[],[],[],[],[],[],[],[],[]]
    minR2  = 22.0
    maxR2  = 11.0
    for di3 in range(len(CA3[0])):
        if maxR2 < CA3[8][di3] and  CA3[8][di3] < minR2:
            for dj3 in range(len(CA3)):
                nl3[dj3].append(CA3[dj3][di3])
    return nl3
def search_CAT_UCAC4_AutoObs(cRA3,cDec3,Rad3,minR2,maxR2):
    CA3  = search_CAT_UCAC4(cRA3,cDec3,Rad3)
    nl3  = [[],[],[],[],[],[],[],[],[],[]]
    for di3 in range(len(CA3[0])):
        if minR2 < CA3[8][di3] and  CA3[8][di3] < maxR2:
            for dj3 in range(len(CA3)):
                nl3[dj3].append(CA3[dj3][di3])
    return nl3
#################################################################
#################################################################
#################################################################

if __name__ == "__main__":
    #cRA1     = 166.695791
    #cDec1    = 72.586586
    #Rad1     = 0.049050
    if ":" in sys.argv[1]:
        cRA1     = ra2deg(sys.argv[1])
        cDec1    = dec2deg(sys.argv[2])
    else:
        cRA1     = float(sys.argv[1])
        cDec1    = float(sys.argv[2])
    Rad1     = float(sys.argv[3])
    CA       = search_CAT_UCAC4(cRA1,cDec1,Rad1)
    print("# Center [degree]     : %s %s"%(cRA1,cDec1))
    print("#        [sexagesimal]: %s %s"%(deg2ra(cRA1),deg2dec(cDec1)))
    print("# Radius [degree]     : %s"%Rad1)
    print("#        [minute]     : %s"%(Rad1*60))
    print("# ID R.A. Dec. Vmag Jmag Hmag Kmag Bmag Rmag Imag")
    for j in range(len(CA[0])):
        print(CA[0][j],CA[1][j],CA[2][j],CA[3][j],CA[4][j],CA[5][j],CA[6][j],CA[7][j],CA[8][j],CA[9][j])


